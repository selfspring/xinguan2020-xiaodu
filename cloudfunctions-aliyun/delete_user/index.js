'use strict';
const db = uniCloud.database()
const role = 0
exports.main = async (event, context) => {
	let {
		res,
		user
	} = await cheak(event)
	if (res.code <= 1111) {

		if (event.uid != event.data._id) {
			/*--------------演示系统特殊代码------------*/
			var admindata = await db.collection('user').where({
				'_id': event.data._id,

			}).get()
			if (admindata.data[0].name == 'admin') {
				res.success = false;
				res.message = '演示系统无修改admin权限，请联系管理员';
				return res
			}
			/*--------------演示系统特殊代码------------*/
			const delete_location = await db.collection('user').where({
				_id: event.data._id
			}).remove()
			res.data = delete_location.data
			res.success = true
		} else {
			res.success = false
			res.message = '不得删除自己！'
		}

	}
	return res
}

async function cheak(e) {
	var res = {
		"code": "0000",
		"data": {}
	}
	var user = await db.collection('user').where({
		'_id': e.uid,
		'token': e.token
	}).get()
	user = user.data[0];
	if (user) {
		if (role != user.role) {
			res.code = 4003
		}
	} else {
		res.code = 4001;
	}
	return {
		res,
		user
	}
}
